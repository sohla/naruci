-- MariaDB dump 10.17  Distrib 10.4.10-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: web_baza
-- ------------------------------------------------------
-- Server version	10.4.10-MariaDB-1:10.4.10+maria~bionic-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administratori`
--

DROP TABLE IF EXISTS `administratori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administratori` (
  `ime` varchar(50) DEFAULT NULL,
  `prezime` varchar(50) DEFAULT NULL,
  `pol` varchar(50) DEFAULT NULL,
  `korisnicko` varchar(50) DEFAULT NULL,
  `lozinka` varchar(50) DEFAULT NULL,
  `JMBG` varchar(50) DEFAULT NULL,
  `plata` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administratori`
--

LOCK TABLES `administratori` WRITE;
/*!40000 ALTER TABLE `administratori` DISABLE KEYS */;
INSERT INTO `administratori` VALUES ('karadordje','karadjordje','muski','karadjordje','karadjordje123','012930192309','100000','18050202'),('milos','curcic','muski','sohla','sohla123','01010398','156000','4442069');
/*!40000 ALTER TABLE `administratori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artikli`
--

DROP TABLE IF EXISTS `artikli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artikli` (
  `restoran` varchar(50) DEFAULT NULL,
  `naziv` varchar(50) DEFAULT NULL,
  `cena` double DEFAULT NULL,
  `opis` varchar(50) DEFAULT NULL,
  `kolicina` double DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artikli`
--

LOCK TABLES `artikli` WRITE;
/*!40000 ALTER TABLE `artikli` DISABLE KEYS */;
INSERT INTO `artikli` VALUES ('1069375','karbonara',400,'pasta,sos,meso',350,'915608141'),('1069375','margarita',400,'testo,sos',670,'916516971'),('12229375','gulas',500,'kuvano jelo',390,'67012'),('12229375','sarma',350,'simbioza',490,'21581'),('888888888','pas',500,'av av',450,'123123'),('888888888','macka',500,'mau mau',400,'325231');
/*!40000 ALTER TABLE `artikli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dostavljaci`
--

DROP TABLE IF EXISTS `dostavljaci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dostavljaci` (
  `ime` varchar(50) DEFAULT NULL,
  `prezime` varchar(50) DEFAULT NULL,
  `pol` varchar(50) DEFAULT NULL,
  `korisnicko` varchar(50) DEFAULT NULL,
  `lozinka` varchar(50) DEFAULT NULL,
  `registracija` varchar(50) DEFAULT NULL,
  `tipVozila` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dostavljaci`
--

LOCK TABLES `dostavljaci` WRITE;
/*!40000 ALTER TABLE `dostavljaci` DISABLE KEYS */;
INSERT INTO `dostavljaci` VALUES ('nadezda','petrovic','zenski','nadezda','nadezda123','NS022IC','automobil','1919191'),('nikola','tesla','muski','nikola','nikola123','AC024DC','bicikl','18671944'),('pregrad','sindjelic','muski','predrag','predrag123','NS001RC','KOMBI','121244');
/*!40000 ALTER TABLE `dostavljaci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kupci`
--

DROP TABLE IF EXISTS `kupci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kupci` (
  `ime` varchar(50) DEFAULT NULL,
  `prezime` varchar(50) DEFAULT NULL,
  `pol` varchar(50) DEFAULT NULL,
  `korisnicko` varchar(50) DEFAULT NULL,
  `lozinka` varchar(50) DEFAULT NULL,
  `adresa` varchar(50) DEFAULT NULL,
  `brTelefona` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kupci`
--

LOCK TABLES `kupci` WRITE;
/*!40000 ALTER TABLE `kupci` DISABLE KEYS */;
INSERT INTO `kupci` VALUES (NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('petar','petrovic','muski','petar','petar123','petra petrovica 1','0632949678','1238434'),('ivan','ivanovic','muski','ivan','ivan123','radnicka 122','064564565','979567894'),('jovan','jovanovic','muski','jovan','jovan123','kralja petra 22','061522265','12312378');
/*!40000 ALTER TABLE `kupci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `porudzbine`
--

DROP TABLE IF EXISTS `porudzbine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `porudzbine` (
  `jelo` varchar(50) DEFAULT NULL,
  `datum` datetime DEFAULT NULL,
  `dostavljac` varchar(50) DEFAULT NULL,
  `kupac` varchar(50) DEFAULT NULL,
  `restoran` varchar(50) DEFAULT NULL,
  `broj` int(11) DEFAULT NULL,
  `stanje` varchar(25) DEFAULT NULL,
  `cena` double DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `porudzbine`
--

LOCK TABLES `porudzbine` WRITE;
/*!40000 ALTER TABLE `porudzbine` DISABLE KEYS */;
INSERT INTO `porudzbine` VALUES ('915608141','2019-07-12 12:12:00','1919191','1238434','1069375',11,'dostavljeno',350,'1111111'),('916516971','2019-08-10 20:12:00','18671944','979567894','1069375',22,'ceka',400,'2222222'),('67012','2019-05-15 08:56:00','121244','12312378','12229375',33,'otkazano',500,'33333');
/*!40000 ALTER TABLE `porudzbine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restorani`
--

DROP TABLE IF EXISTS `restorani`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorani` (
  `naziv` varchar(50) DEFAULT NULL,
  `adresa` varchar(50) DEFAULT NULL,
  `kategorija` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restorani`
--

LOCK TABLES `restorani` WRITE;
/*!40000 ALTER TABLE `restorani` DISABLE KEYS */;
INSERT INTO `restorani` VALUES ('restoran 1','bulevar jase tomica 3','italijanska kuhinja','1069375'),('gregors','novosadskog sajma 56','domaca kuhinja','12229375'),('chi na','joa minga 224','kineska kuhinja kuhinja','888888888');
/*!40000 ALTER TABLE `restorani` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-17 10:14:46
