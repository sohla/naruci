package obrada;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.UUID;


import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import DAO.PorudzbinaDAO;
import modeli.Porudzbina;

/**
 * Servlet implementation class KorpaServlet
 */
@WebServlet("/KorpaServlet")
public class KorpaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KorpaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);

		try {
			String action = request.getParameter("action");
			switch (action) {
			case "add": {
				
				String korisnik = (String) request.getSession().getAttribute("loggedUserId");
				
				String restoran = request.getParameter("restoran");
				String naziv = request.getParameter("naziv");
				double cena = Double.parseDouble(request.getParameter("cena"));
				//double cena = request.getParameter("cena");
				String opis = request.getParameter("opis");
				String kolicina = request.getParameter("kolicina");
				String idJela = request.getParameter("id");
				Integer broj = Integer.parseInt(request.getParameter("broj"));


			    java.util.Date date = Calendar.getInstance().getTime();  
			    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
			    String datum = dateFormat.format(date);  
			    
			    String uniqueID = UUID.randomUUID().toString();
			    	
				request.getSession().setAttribute("izabranRestoran", restoran);
				
				Porudzbina porudzbina = new Porudzbina(idJela,datum,null,korisnik,restoran,broj,null,cena,uniqueID);
				PorudzbinaDAO.createPorudzbina(porudzbina);
			}
			case "update": {
			
				String id = request.getParameter("id");
				String stanje = request.getParameter("stanje");
				
				PorudzbinaDAO.updatePorudzbina(stanje, id);
			
			}
			}
			
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		

	}

}
