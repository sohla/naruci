package obrada;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.AdminDAO;
import DAO.DostavljacDAO;
import DAO.JeloDAO;
import DAO.KupacDAO;
import modeli.Administrator;
import modeli.Dostavljac;
import modeli.Kupac;

/**
 * Servlet implementation class DostavljacServlet
 */
@WebServlet("/DostavljacServlet")
public class DostavljacServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DostavljacServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		
		Dostavljac dostavljac = DostavljacDAO.getDostavljacID(id);
		String json = new Gson().toJson(dostavljac);
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    
	    response.getWriter().write(json);
	    System.out.print("======aaaa====");
	    System.out.print(id);
	    System.out.print("asdasdasdasda");
	    System.out.print(json);
	    System.out.print("asdasdasdasda");
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String ime = request.getParameter("imeDostavljaca");
					String prezime = request.getParameter("prezimeDostavljaca");
					String pol = request.getParameter("polDostavljaca");
					String korisnicko = request.getParameter("korisnickoDostavljaca");
					String lozinka= request.getParameter("lozinkaDostavljaca");
					String registracija = request.getParameter("registracijaDostavljaca");
					String tipVozila = request.getParameter("tipVozilaDostavljaca");
					String id = request.getParameter("id");
					
					Dostavljac dostavljac = new Dostavljac(ime, prezime,pol, korisnicko,lozinka,registracija, tipVozila, id);
					DostavljacDAO.createDostavljac(dostavljac);
				}
				case "update": {
					
					String ime = request.getParameter("imeDostavljaca");
					String prezime = request.getParameter("prezimeDostavljaca");
					String pol = request.getParameter("polDostavljaca");
					String korisnicko = request.getParameter("korisnickoDostavljaca");
					String lozinka= request.getParameter("lozinkaDostavljaca");
					String registracija = request.getParameter("registracijaDostavljaca");
					String tipVozila = request.getParameter("tipVozilaDostavljaca");
					String id = request.getParameter("id");
					
					Dostavljac dostavljac = DostavljacDAO.getDostavljacID(id);
					
					dostavljac.setIme(ime);
					dostavljac.setPrezime(prezime);
					dostavljac.setPol(pol);
					dostavljac.setKorisnicko(korisnicko);
					dostavljac.setLozinka(lozinka);
					dostavljac.setRegistracija(registracija);
					dostavljac.setTipVozila(tipVozila);
					dostavljac.setId(id);

					
				}
				case "delete": {
					
					String id = request.getParameter("id");
					DostavljacDAO.deleteDostavljac(id);
					
				}
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
