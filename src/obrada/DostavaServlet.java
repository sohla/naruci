package obrada;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.DostavljacDAO;
import DAO.PorudzbinaDAO;
import modeli.Dostavljac;
import modeli.Porudzbina;

/**
 * Servlet implementation class DostavaServlet
 */
@WebServlet("/DostavaServlet")
public class DostavaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = (String) request.getSession().getAttribute("loggedUserId");
		System.out.print(id);
		

		String idP = request.getParameter("idP");
		
		System.out.print("\n");
		System.out.print("---");
		System.out.print("\n");
		System.out.print(idP);
		System.out.print("\n");
		System.out.print("---");
		
		ArrayList<Porudzbina> porudzbine = PorudzbinaDAO.getPorudzbinaCekanje();
		String porudzbineJson = new Gson().toJson(porudzbine);
		System.out.print(porudzbineJson);
		
		ArrayList<Porudzbina> porudzbineDostavljeno = PorudzbinaDAO.getPorudzbinaStare(id);
		String porudzbineStare = new Gson().toJson(porudzbineDostavljeno);
		System.out.print(porudzbineJson);
		
		Porudzbina porudzbina = PorudzbinaDAO.getPorudzbina(idP);
		String porudzbinaId = new Gson().toJson(porudzbina);
		System.out.print("\n");
		System.out.print("\n");
		System.out.print("--- -- - - - -- ---- ");
		System.out.print(porudzbinaId);
		System.out.print("\n");
		
		String Json = "["+porudzbineJson+","+porudzbineStare+","+porudzbinaId+"]";

		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(Json);
		
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
