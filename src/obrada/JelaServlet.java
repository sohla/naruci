package obrada;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.JeloDAO;
import modeli.Artikal;

/**
 * Servlet implementation class JelaServlet
 */
@WebServlet("/JelaServlet")
public class JelaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public JelaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		
		Artikal jelo = JeloDAO.getArtikal(id);
		String  jeloJson = new Gson().toJson(jelo);
		System.out.print(jelo);
		
		ArrayList<Artikal> menu = JeloDAO.getRestoranMenu(id);
		String  menuJson = new Gson().toJson(menu);
		System.out.print(jelo);
		
		String Json = "["+jeloJson+","+menuJson+"]";
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    
	    response.getWriter().write(Json);

	    
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String restoran = request.getParameter("restoran");
					String naziv = request.getParameter("naziv");
					Double cena = Double.parseDouble(request.getParameter("cena"));
					String opis = request.getParameter("opis");
					Double  kolicina = Double.parseDouble(request.getParameter("kolicina"));
					String id = request.getParameter("id");
					
					Artikal artikal = new Artikal(restoran, naziv, cena, opis, kolicina,id);
					JeloDAO.createArtikal(artikal);
				}
				case "update": {
					
					String restoran = request.getParameter("restoran");
					String naziv = request.getParameter("naziv");
					Double cena = Double.parseDouble(request.getParameter("cena"));
					String opis = request.getParameter("opis");
					Double  kolicina = Double.parseDouble(request.getParameter("kolicina"));
					String id = request.getParameter("id");
					
					System.out.print(restoran);
					
					Artikal artikal = JeloDAO.getArtikal(id);
					
					System.out.print(artikal);
					
					Artikal artikalUpdate = new Artikal(restoran, naziv,cena, opis,kolicina,id);
					
					JeloDAO.updateArtikal(artikalUpdate);
					
/*					
					artikal.setRestoran(restoran);
					artikal.setNaziv(naziv);
					artikal.setCena(cena);
					artikal.setOpis(opis);
					artikal.setKolicina(kolicina);
					artikal.setId(id);		
*/					
				}
				case "delete": {
					
					String id = request.getParameter("id");
					JeloDAO.deleteArtikal(id);
					
				}
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
		
		String restoran = request.getParameter("restoran");
		System.out.println(restoran);
		
		if(restoran=="111") {
			System.out.println("a");
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("DodajArtikal.html");  
	        rd.include(request,response);  
	        System.out.println("restoran");
		}
		
	}

}
