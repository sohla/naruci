package obrada;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.DostavljacDAO;
import DAO.KupacDAO;
import modeli.Dostavljac;
import modeli.Kupac;

/**
 * Servlet implementation class Kupac
 */
@WebServlet("/KupacServlet")
public class KupacServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		
		Kupac kupac = KupacDAO.getKupacID(id);
		String json = new Gson().toJson(kupac);
		
	    System.out.print("asdasdasdasda==========");
	    System.out.print(json);
	    System.out.print("==========asdasdasdasda");
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String ime = request.getParameter("imeKupca");
					String prezime = request.getParameter("prezimeKupca");
					String pol = request.getParameter("polKupca");
					String korisnicko = request.getParameter("korisniKupca");
					String lozinka= request.getParameter("lozinkaKupca");
					String adresa = request.getParameter("adresaKupca");
					String brTelefona = request.getParameter("brTelefonaKupca");
					String id = request.getParameter("id");
					
					Kupac kupac = new Kupac(ime, prezime,pol, korisnicko,lozinka,adresa, brTelefona, id);
					KupacDAO.createKupac(kupac);
				}
				case "update": {
					
					String ime = request.getParameter("imeKupca");
					String prezime = request.getParameter("prezimeKupca");
					String pol = request.getParameter("polKupca");
					String korisnicko = request.getParameter("korisniKupca");
					String lozinka= request.getParameter("lozinkaKupca");
					String adresa = request.getParameter("adresaKupca");
					String brTelefona = request.getParameter("brTelefonaKupca");
					String id = request.getParameter("id");
					
					Kupac kupac = KupacDAO.getKupacID(id);
					
					kupac.setIme(ime);
					kupac.setPrezime(prezime);
					kupac.setPol(pol);
					kupac.setKorisnicko(korisnicko);
					kupac.setLozinka(lozinka);
					kupac.setAdresa(adresa);
					kupac.setBrTelefona(brTelefona);
					kupac.setId(id);

					
				}
				case "delete": {
					
					String id = request.getParameter("id");
					KupacDAO.deleteKupac(id);
					
				}
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
