package modeli;

public class Osoba {
		
		protected String ime;
		protected String prezime;
		protected String pol;
		protected String korisnicko;
		protected String lozinka;

		public Osoba() {
	
			this.ime = "";
			this.prezime = "";
			this.pol = "";
			this.korisnicko = "";
			this.lozinka = "";
			}
		
		public Osoba( String ime, String prezime, String pol, String korisnicko, String lozinka) {
	
			this.ime = ime;
			this.prezime = prezime;
			this.pol = pol;
			this.korisnicko = korisnicko;
			this.lozinka = lozinka;
		}
		public Osoba(Osoba original) {
		
			this.ime = original.ime;
			this.prezime = original.prezime;
			this.pol = original.pol;
			this.korisnicko = original.korisnicko;
			this.lozinka = original.lozinka;
		}
/*
		public String getTip() {
			return tip;
		}

		public void setTip(String tip) {
			this.tip = tip;
		}
*/
		public String getIme() {
			return ime;
		}

		public void setIme(String ime) {
			this.ime = ime;
		}

		public String getPrezime() {
			return prezime;
		}

		public void setPrezime(String prezime) {
			this.prezime = prezime;
		}

		public String getPol() {
			return pol;
		}

		public void setPol(String pol) {
			this.pol = pol;
		}

		public String getKorisnicko() {
			return korisnicko;
		}

		public void setKorisnicko(String korisnicko) {
			this.korisnicko = korisnicko;
		}

		public String getLozinka() {
			return lozinka;
		}

		public void setLozinka(String lozinka) {
			this.lozinka = lozinka;
		}

		@Override
		public String toString() {
			return "Osoba [ime=" + ime + ", prezime="
					+ prezime + ", pol=" + pol + ", korisnicko=" + korisnicko
					+ ", lozinka=" + lozinka + "]";
		}
		
	}

