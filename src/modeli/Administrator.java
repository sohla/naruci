package modeli;

public class Administrator extends Osoba  {
	
	private String JMBG;	
	private double plata;
	private String id;
//extends Osoba	
	public Administrator() {
		this.ime = "";
		this.prezime = "";
		this.pol = "";
		this.korisnicko = "";
		this.lozinka = "";	
		this.JMBG = "";
		this.plata = 0;
		this.id = "";
	}
	public Administrator(String ime, String prezime, String pol,String korisnicko,String lozinka,String JMBG, double plata, String id) {
		super(ime,prezime,pol,korisnicko,lozinka);
		this.JMBG = JMBG;	
		this.plata = plata;
		this.id = id;
	}
	public Administrator(Administrator original) {
		super(original);
		//this.ime = original.ime;
		this.JMBG = original.JMBG;
		this.plata = original.plata;
		this.id = original.id;
	}
	

	public String getJMBG() {
		return JMBG;
	}
	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}
	public double getPlata() {
		return plata;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setPlata(double plata) {
		this.plata = plata;
	}
	@Override
	public String toString() {
		return "Administrator [ime=" + ime + ", JMBG=" + JMBG + ", plata=" + plata + ", id=" + id + ", prezime="
				+ prezime + ", pol=" + pol + ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}
	
}
