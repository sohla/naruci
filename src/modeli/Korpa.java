package modeli;

import java.util.ArrayList;

public class Korpa {
	
	private ArrayList<Artikal> korpa;
	
	public Korpa() {
		korpa = new ArrayList<Artikal>();
	}
	public void dodaj(Artikal artikal,Integer broj) {
		korpa.add(new Artikal());
	}
	public void obrisi(int index) {
		korpa.remove(index);
	}
	public ArrayList<Artikal> getAll() {
		return korpa;
	}
	
	public double getTotalPrice() {
		double totalPrice = 0;
		for (Artikal artikal: korpa) {
			totalPrice += artikal.getCena();
		}
		
		return totalPrice;
	}

}
