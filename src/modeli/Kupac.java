package modeli;

public class Kupac extends Osoba {
	private String adresa;
	private String brTelefona;
	private String id;

	public Kupac() {
		this.ime = "";
		this.prezime = "";
		this.pol = "";
		this.korisnicko = "";
		this.lozinka = "";		
		this.adresa = "";
		this.brTelefona = "";	
		this.id = "";
	}
	
	public Kupac(String ime, String prezime, String pol,String korisnicko,String lozinka,String adresa,String id, String brTelefona){
		super(ime,prezime,pol,korisnicko,lozinka);
		this.adresa = adresa;
		this.brTelefona = brTelefona;
		this.id = id;
	}
	public Kupac(Kupac original){
		super(original);
		this.adresa = original.adresa;
		this.brTelefona = original.brTelefona;
		this.id = original.id;
	}


	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(String brTelefona) {
		this.brTelefona = brTelefona;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Kupac [adresa=" + adresa + ", brTelefona=" + brTelefona + ", id=" + id + ", ime=" + ime + ", prezime="
				+ prezime + ", pol=" + pol + ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}
	
}


