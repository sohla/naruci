package DAO;
import java.util.List;

import DAO.Connect;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.ResultSet;

import modeli.Administrator;
import modeli.Dostavljac;
import java.sql.PreparedStatement;

public class DostavljacDAO {
	
	private static ArrayList<Dostavljac> dostavljaci; 
	
	public DostavljacDAO() {
		
		this.dostavljaci = new ArrayList<Dostavljac>();
		
	}
	
	public static ArrayList<Dostavljac> getAllDostavljaci() {
		ArrayList<Dostavljac> dostavljaci= new ArrayList<Dostavljac>();
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from dostavljaci");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Dostavljac d = new Dostavljac();
				d.setIme(rs.getString(1));
				d.setPrezime(rs.getString(2));
				d.setPol(rs.getString(3));
				d.setKorisnicko(rs.getString(4));
				d.setLozinka(rs.getString(5));
				d.setRegistracija(rs.getString(6));
				d.setTipVozila(rs.getString(7));
				d.setId(rs.getString(8));
				dostavljaci.add(d);
			}
			//con.close();
		}catch(Exception e) {e.printStackTrace();}
		
		return dostavljaci;
		
	}
	
	
	public static Dostavljac getDostavljac(String userName, String password) {
		Connection conn = Connect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM dostavljaci WHERE korisnicko = ? AND lozinka = ?";
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, userName);
			pstmt.setString(index++, password);
			System.out.println("pre prinf pstmt");
			System.out.println(pstmt);
			System.out.println("posle ptsmt");
			rset = pstmt.executeQuery();
			System.out.println("executeQueary");
			if (rset.next()) {
				//Role role = Role.valueOf(rset.getString(1));
				String ime = String.valueOf(rset.getString(1));
				String prezime = String.valueOf(rset.getString(2));
				String pol = String.valueOf(rset.getString(3));
				String korisnicko = String.valueOf(rset.getString(4));
				String lozinka = String.valueOf(rset.getString(5));
				String registracija = String.valueOf(rset.getString(6));
				String tipVozila = String.valueOf(rset.getString(7));
				String id = String.valueOf(rset.getString(8));
				
				return new Dostavljac(ime, prezime,pol ,korisnicko,lozinka,registracija ,tipVozila, id);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		//	try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
		//	try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
		//	try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	
	
	
	public static Dostavljac getDostavljacByU(String korisnicko) {
		
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from dostavljaci where koriscnicko =?");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				//Role role = Role.valueOf(rset.getString(1));
				String ime = String.valueOf(rs.getString(1));
				String prezime = String.valueOf(rs.getString(2));
				String pol = String.valueOf(rs.getString(3));
				String korisnickoIme = String.valueOf(rs.getString(4));
				String lozinka = String.valueOf(rs.getString(5));
				String registracija = String.valueOf(rs.getString(6));
				String tipVozila = String.valueOf(rs.getString(7));
				String id = String.valueOf(rs.getString(8));
				return new Dostavljac(ime, prezime, pol, korisnickoIme, lozinka, registracija, tipVozila,id);
			}
		//	con.close();
		}catch(Exception e) {e.printStackTrace();}
		return null;
		
	}
	
	public static boolean createDostavljac(Dostavljac d) {
		Connection con = Connect.getConnection();

		try {
			PreparedStatement ps = con.prepareStatement("insert into dostavljaci values(?,?,?,?,?,?,?,?)");
			ps.setString(1, d.getIme());
			ps.setString(2, d.getPrezime());
			ps.setString(3, d.getPol());
			ps.setString(4, d.getKorisnicko());
			ps.setString(5, d.getLozinka());
			ps.setString(6, d.getRegistracija());
			ps.setString(7, d.getTipVozila());
			ps.setString(8, d.getId());
			return ps.executeUpdate() == 1;
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean updateDostavljac(Dostavljac d) {
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("update dostavljaci set ime=?,prezime=?,pol=?,korisnicko=?,lozinka=?, registracija=?, tipVozila=? where id=?");
			ps.setString(1, d.getIme());
			ps.setString(2, d.getPrezime());
			ps.setString(3, d.getPol());
			ps.setString(4, d.getKorisnicko());
			ps.setString(5, d.getLozinka());
			ps.setString(6, d.getRegistracija());
			ps.setString(7, d.getTipVozila());
			ps.setString(8, d.getId());
			return ps.executeUpdate() == 1;
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}
	
	
	public static boolean deleteDostavljac(String id) {
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("delete from dostavljaci where id=?");
			ps.setString(1, id);
			return ps.executeUpdate() == 1;
		
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}
	
	
	public  Dostavljac loginDostavljac(String korisnickoIme, String sifra) {
		for (Dostavljac dostavljac: dostavljaci) {
			if(dostavljac.getKorisnicko().equalsIgnoreCase(korisnickoIme) && dostavljac.getLozinka().equalsIgnoreCase(sifra)) {
				return dostavljac;
			}
		}
		return null;
	}
	
	public void removeDostavljac(Dostavljac dostavljac) {
		
		dostavljaci.remove(dostavljac);
	}
	
	public Dostavljac getDostavljac(String korisnicko) {
		// TODO Auto-generated method stub
		for(Dostavljac dostavljac : dostavljaci) {
			if(dostavljac.getKorisnicko().equals(korisnicko)) {
				return dostavljac;
			}			
		}
		return null;
	}

	public static Dostavljac getDostavljacID(String id) {
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from dostavljaci where id =?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				//Role role = Role.valueOf(rset.getString(1));
				String ime = String.valueOf(rs.getString(1));
				String prezime = String.valueOf(rs.getString(2));
				String pol = String.valueOf(rs.getString(3));
				String korisnicko = String.valueOf(rs.getString(4));
				String lozinka = String.valueOf(rs.getString(5));
				String registracija = String.valueOf(rs.getString(6));
				String tipVozila = String.valueOf(rs.getString(7));
				String idDostavljaca = String.valueOf(rs.getString(8));
				
				return new Dostavljac(ime, prezime, pol, korisnicko, lozinka, registracija, tipVozila, idDostavljaca);


			}
			//con.close();
		}catch(Exception e) {e.printStackTrace();}
		return null;
	} 
	
}