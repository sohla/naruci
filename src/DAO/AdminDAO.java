package DAO;
import java.util.List;

import DAO.Connect;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.ResultSet;
import modeli.Administrator;
import modeli.Artikal;
import modeli.Kupac;

import java.sql.PreparedStatement;

public class AdminDAO {
	
	
	private static ArrayList<Administrator> administratori; 
	
	public AdminDAO() {	
	
		this.administratori = new ArrayList<Administrator>();
		
	}
	
		public static ArrayList<Administrator> getAllAdministratori() {
			ArrayList<Administrator> administratori = new ArrayList<Administrator>();
			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("select * from administratori");
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					Administrator d = new Administrator("","","","","","",0,"");
					d.setIme(rs.getString(1));
					d.setPrezime(rs.getString(2));
					d.setPol(rs.getString(3));
					d.setKorisnicko(rs.getString(4));
					d.setLozinka(rs.getString(5));
					d.setJMBG(rs.getString(6));
					d.setPlata(rs.getDouble(7));
					d.setId(rs.getString(8));
					
					administratori.add(d);
				}
				//con.close();
			}catch(Exception e) {e.printStackTrace();}
			
			return administratori;
			
		}
		
		public static Administrator getAdministrator(String userName, String password) {
			Connection conn = Connect.getConnection();
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			try {
				String query = "SELECT * FROM administratori WHERE korisnicko = ? AND lozinka = ?";
				pstmt = conn.prepareStatement(query);
				int index = 1;
				pstmt.setString(index++, userName);
				pstmt.setString(index++, password);
				System.out.println("pre prinf pstmt");
				System.out.println(pstmt);
				System.out.println("posle ptsmt");
				rset = pstmt.executeQuery();
				System.out.println("executeQueary");
				if (rset.next()) {
					//Role role = Role.valueOf(rset.getString(1));
					String ime = String.valueOf(rset.getString(1));
					String prezime = String.valueOf(rset.getString(2));
					String pol = String.valueOf(rset.getString(3));
					String korisnicko = String.valueOf(rset.getString(4));
					String lozinka = String.valueOf(rset.getString(5));
					String JMBG = String.valueOf(rset.getString(6));
					Double plata = Double.valueOf(rset.getString(7));
					String id = String.valueOf(rset.getString(8));
					
					return new Administrator(ime, prezime,pol ,korisnicko,lozinka,JMBG ,plata, id);
					
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
			//	try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			//	try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			//	try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
			}

			return null;
		}
	
		
		public static Administrator getAdministratorByU(String korisnicko) {

			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("select * from administratori where id =?");
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					String ime = String.valueOf(rs.getString(1));
					String prezime = String.valueOf(rs.getString(2));
					String pol = String.valueOf(rs.getString(3));
					String korisnickoIme = String.valueOf(rs.getString(4));
					String lozinka = String.valueOf(rs.getString(5));
					String JMBG = String.valueOf(rs.getString(6));
					Double plata = Double.valueOf(rs.getString(7));
					String id = String.valueOf(rs.getString(8));
					return new Administrator(ime, prezime,pol ,korisnickoIme,lozinka, JMBG, plata, id);
				}
			//	con.close();
			}catch(Exception e) {e.printStackTrace();}
			return null;
			
		}
		
		public static boolean createAdministrator(Administrator d) {
			int status = 0;
			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("insert into administratori values(?,?,?,?,?,?,?,?)");
				ps.setString(1, d.getIme());
				ps.setString(2, d.getPrezime());
				ps.setString(3, d.getPol());
				ps.setString(4, d.getKorisnicko());
				ps.setString(5, d.getLozinka());
				ps.setString(6, d.getJMBG());
				ps.setDouble(7, d.getPlata());
				ps.setString(8, d.getId());
				return ps.executeUpdate() == 1;
				
			}catch(Exception e) {e.printStackTrace();}
			return false;
		}
		
		public static boolean updateAdministrator(Administrator d) {
			int status = 0;
			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("update administratori set ime=?,pol=?,prezime=?,korisnicko=?,lozinka=?, JMBG=?, plata=? where id=?");
				ps.setString(1, d.getIme());
				ps.setString(2, d.getPrezime());
				ps.setString(3, d.getPol());
				ps.setString(4, d.getKorisnicko());
				ps.setString(5, d.getLozinka());
				ps.setString(6, d.getJMBG());
				ps.setDouble(7, d.getPlata());
				ps.setString(8, d.getId());
				return ps.executeUpdate() == 1;
				
				//int rs = ps.executeUpdate();
			}catch(Exception e) {e.printStackTrace();}
			return false;
		}
		public static boolean deleteAdministrator(String id) {

			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("delete from administratori where id=?");
				ps.setString(1, id);
				return ps.executeUpdate() == 1;
			}catch(Exception e) {e.printStackTrace();}
			return false;
		}
		
		
		public Administrator loginAdministrator(String korisnickoIme, String sifra) {
			for (Administrator administrator : administratori) {
				if(administrator.getKorisnicko().equalsIgnoreCase(korisnickoIme) && administrator.getLozinka().equalsIgnoreCase(sifra)) {
					return administrator;
				}
			}
			return null;
		}
		
		public void removeAdministrator(Administrator administrator) {
			
			administratori.remove(administrator);
		}
		
		public Administrator getAdministrator(String korisnicko) {
			// TODO Auto-generated method stub
			for(Administrator administrator : administratori) {
				if(administrator.getKorisnicko().equals(korisnicko)) {
					return administrator;
				}			
			}
			return null;
		}

		public static Administrator getAdministratorID(String id) {
			// TODO Auto-generated method stub
			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("select * from administratori where id =?");
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					//Role role = Role.valueOf(rset.getString(1));
					String ime = String.valueOf(rs.getString(1));
					String prezime = String.valueOf(rs.getString(2));
					String pol = String.valueOf(rs.getString(3));
					String korisnicko = String.valueOf(rs.getString(4));
					String lozinka = String.valueOf(rs.getString(5));
					String JMBG = String.valueOf(rs.getString(6));
					Double plata = Double.parseDouble(String.valueOf(rs.getString(7)));
					String idAdmina = String.valueOf(rs.getString(8));
					
					return new Administrator(ime, prezime, pol, korisnicko, lozinka, JMBG, plata, idAdmina);

	
				}
			//	con.close();
			}catch(Exception e) {e.printStackTrace();}
			return null;
		} 
		
		
		
		
}