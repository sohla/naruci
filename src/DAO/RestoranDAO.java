package DAO;
import java.sql.Connection;
import java.util.List;

import DAO.Connect;

import java.util.ArrayList;
import java.util.HashMap;

import modeli.Artikal;
import modeli.Restoran;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RestoranDAO {

		static Connection con =null;
		
		public static ArrayList<Restoran> getAllRestoran() {
			ArrayList<Restoran> list = new ArrayList<Restoran>();
			con = Connect.getConnection();
			
			try {				
				PreparedStatement ps = con.prepareStatement("select * from restorani");
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					Restoran r = new Restoran();
					r.setNaziv(rs.getString(1));
					r.setAdresa(rs.getString(2));
					r.setKategorija(rs.getString(3));
					r.setId(rs.getString(4));
					list.add(r);
				}
		
				
			}catch(Exception e) {e.printStackTrace();}
			System.out.print("\n");
			System.out.print(list);
			System.out.print("\n");
			System.out.print("======");
			return list;
			
			
		}
		//String naziv,String adresa,
		public static ArrayList<Restoran> getRestoran(String kategorija) {
			ArrayList<Restoran> restorani = new ArrayList<Restoran>();
			con = Connect.getConnection();
			
			PreparedStatement pstmt =  null;
			ResultSet rset = null;
			//naziv LIKE ? AND adresa LIKE ?
			try {
				System.out.println(kategorija);
				String  query = "SELECT * FROM restorani WHERE kategorija=?";
				System.out.println(query);
				pstmt = con.prepareStatement(query);
				System.out.println(pstmt);
				int index = 1;
		//		pstmt.setString(index++, naziv);
		//		pstmt.setString(index++, adresa);
			
				pstmt.setString(1, kategorija);
				
				rset = pstmt.executeQuery();
				
				if (rset.next()) {
					index = 1;		
					String n = rset.getString(index++);
					String a = rset.getString(index++);
					String k = rset.getString(index++);
					String id = rset.getString(index++);
					Restoran restoran = new Restoran(n,a,k,id);
					//System.out.print(restoran);
					restorani.add(restoran);
				}
				
			}catch(Exception ex) {
				ex.printStackTrace();
			}finally {
				//try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
				//try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
				//try {con.close();} catch (Exception ex1) {ex1.printStackTrace();}			
			}
			
			return restorani;
			
		}
		
		
		public static boolean updateRestoran(Restoran r) {
		//Restoran [naziv=chi na, adresa=joa minga 224, kategorija=kineska kuhinja kuhinja, id=888888888]	
			con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("update restorani set naziv=?, adresa=?, kategorija=? where id =?");
				ps.setString(1, r.getNaziv());
				ps.setString(2, r.getAdresa());
				ps.setString(3, r.getKategorija());
				ps.setString(4, r.getId());
				return ps.executeUpdate() == 1;
				//con.close();
			}catch(Exception e) {e.printStackTrace();}
			
			return false;
			
		}
		
		public static boolean deleteRestoran(String id) {
			
			con = Connect.getConnection();
			
			try {
				//int status = 0;
				PreparedStatement ps = con.prepareStatement("delete from restorani where id =?");
				ps.setString(1, id);
				return ps.executeUpdate() == 1;
				
			}catch(Exception e) {e.printStackTrace();}
			return false;
			
			//return status;
		}
		
		
		
		public static  boolean createRestoran(Restoran r) {
		
			con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("insert into restorani values(?,?,?,?)");
				ps.setString(1, r.getNaziv());
				ps.setString(2, r.getAdresa());
				ps.setString(3, r.getKategorija());
				ps.setString(4, r.getId());
				return ps.executeUpdate() == 1;
				
			}catch(Exception e) {e.printStackTrace();}
			return false;
		}
		
		
		
		public static Restoran getRestoranID(String id) {
			Connection con = Connect.getConnection();
			try {
				PreparedStatement ps = con.prepareStatement("select * from restorani where id =?");
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					//Role role = Role.valueOf(rset.getString(1));
					String naziv = String.valueOf(rs.getString(1));
					String adresa = String.valueOf(rs.getString(2));
					String kategorija = String.valueOf(rs.getString(3));
					String idRestorana = String.valueOf(rs.getString(4));
					
					return new Restoran (naziv, adresa, kategorija, idRestorana);

				//	String id = String.valueOf(rs.getString(8));
				//	return new Dostavljac(ime, prezime, pol, korisnickoIme, lozinka, registracija, tipVozila,id);
				}
				//con.close();
			}catch(Exception e) {e.printStackTrace();}
			return null;
		}
		
		
		/*
		public static Restoran getRestoranByName() {
			Restoran r = new Restoran(null, null, null,null);
			con = Connect.connect();
			try {
				PreparedStatement ps = con.prepareStatement("select * from restorani where naziv=?");
				ResultSet rs = ps.executeQuery();
				r.setNaziv(rs.getString(1));
				r.setKategorija(rs.getString(2));
				r.setId(rs.getString(4));
				
			}catch(Exception e) {e.printStackTrace();;}
			
			return r;
		}
		
		
		public static  int createRestoran(Restoran r) {
			int status = 0;
			con = Connect.connect();
			try {
				PreparedStatement ps = con.prepareStatement("insert into restorani values(?,?,?,?)");
				ps.setString(1, r.getNaziv());
				ps.setString(2, r.getAdresa());
				ps.setString(3, r.getKategorija());
				ps.setString(4, r.getId());
				int rs = ps.executeUpdate();
				
			}catch(Exception e) {e.printStackTrace();}
			return status;
		}
	
		public static int updateRestoran(Restoran r) {
			int status = 0;
			con = Connect.connect();
			try {
				PreparedStatement ps = con.prepareStatement("update restorani set nazivRestorana=?, adresa=?, kategorija=? where id =?");
				ps.setString(1, r.getNaziv());
				ps.setString(2, r.getAdresa());
				ps.setString(3, r.getKategorija());
				ps.setString(4, r.getId());
				int rs = ps.executeUpdate();
				con.close();
			}catch(Exception e) {e.printStackTrace();}
			
			return status;
			
		}
		
		public static void deleteRestoran(String id) {
			int status = 0;
			con = Connect.connect();
			
			try {
				
				PreparedStatement ps = con.prepareStatement("delete from restorani where id =?");
				ps.setString(1, id);
				int rs = ps.executeUpdate();
				
			}catch(Exception e) {e.printStackTrace();}
			
			//return status;
		}
		*/
}